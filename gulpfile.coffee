gulp   = require 'gulp'
coffee = require 'gulp-coffee'
webpack = require 'gulp-webpack'

gulp.task 'compile', ->
  gulp.src 'src/**/*.coffee'
    .pipe coffee()
    .pipe gulp.dest('built')

gulp.task 'webpack', ->
  gulp.src 'src'
  .pipe webpack {
    entry:
      packed_test7: "./src/test7.coffee"
    output:
      filename: "[name].js"
    resolve:
      root: ['./src']
      extensions: ['', '.js', '.coffee']
    module:
      loaders: [{test: /\.coffee$/, loader: "coffee-loader"}]
  }
  .pipe gulp.dest('built')
