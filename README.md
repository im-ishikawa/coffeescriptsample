# 開発環境構築 for CoffeeScript

### Node.jsとnpmをインストール
https://nodejs.org/

    >node -v
    >npm -v

### CoffeeScriptをインストール

    >npm install coffee-script -g
    >coffee -v

### gulpをインストール
先にpackage.jsonがある場合は`npm install`でpackage.jsonに定義されたパッケージがインストールされる

    >npm install gulp -g
    >gulp -v


## これ以降は必要に応じて

### bowerをインストール
フロントエンド開発用のパッケージ管理ツール

    >npm install bower -g
    >bower -v

### webpackをインストール
モジュールの依存関係を解決するもの

    >npm install webpack -g

### jasmineをインストール
テストツール

    >npm install jasmine -g

### shouldをインストール
テストコードの文法

    >npm install should -g


# 開発環境構築 at SublimeText3

### Better CoffeeScript


# プロジェクト開始（testプロジェクト）

### フォルダ作成

    ~/workspace_coffee/test2
    ~/workspace_coffee/test2/src

### npmを初期化
package.jsonが作られる（手でファイルを作ってもOK）

    >cd ~/workspace_coffee/test2
    >npm init

### gulpをインストール

    >cd ~/workspace_coffee/test2
    >npm install --save-dev gulp gulp-coffee

### webpackをインストール

    >cd ~/workspace_coffee/test2
    >npm install --save-dev gulp gulp-webpack coffee-loader

### gulpfile.coffeeを作成


# 開発

### コーディング
srcフォルダの中にcoffeeファイルを作成する

### コンパイル
各ファイルをコンパイル

    >gulp compile

test7を依存関係を含めてコンパイル

    >gulp webpack

### コマンドラインから実行
コンパイルせずに実行

    >coffee src/xxxx.coffee

コンパイルしたものを実行

    >node built/xxxx.js
