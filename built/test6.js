(function() {
  var myCat3;

  this.Cat3 = (function() {
    function Cat3() {}

    Cat3.prototype.meow = function(s, count) {
      var i, j, ref, result, voice;
      voice = "にゃーん";
      result = "";
      count || (count = 1);
      if (typeof s === "string") {
        voice = s;
      }
      for (i = j = 1, ref = count; 1 <= ref ? j <= ref : j >= ref; i = 1 <= ref ? ++j : --j) {
        result += i + voice;
      }
      return result;
    };

    Cat3.prototype.eat = function() {
      this.length += 0.1;
      return this.weight += 0.1;
    };

    return Cat3;

  })();

  myCat3 = new this.Cat3;

  myCat3.length = 30.5;

  myCat3.weight = 2.5;

  myCat3.eat();

  console.log("6-1:私の猫は" + (myCat3.meow()) + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");

  console.log("6-2:私の猫は" + (myCat3.meow("みゃお")) + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");

  console.log("6-3:私の猫は" + (myCat3.meow("みゃお", 3)) + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");

}).call(this);
