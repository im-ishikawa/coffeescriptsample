(function() {
  var Cat, message, myCat;

  Cat = (function() {
    function Cat() {}

    return Cat;

  })();

  myCat = new Cat;

  myCat.age = 3;

  myCat.weight = 5.1;

  message = "3:うちのネコは" + myCat.age + "歳で、体重は" + myCat.weight + "kgです";

  console.log(message);

}).call(this);
