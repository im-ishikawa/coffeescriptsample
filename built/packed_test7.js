/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var myCat6, test6;

	test6 = __webpack_require__(1);

	myCat6 = new test6.Cat3;

	myCat6.length = 30.5;

	myCat6.weight = 2.5;

	myCat6.eat();

	console.log("7:私の猫は" + (myCat6.meow("みゃお")) + "と鳴き、\n体長は" + myCat6.length + "cm、体重は" + myCat6.weight + "kgです");


/***/ },
/* 1 */
/***/ function(module, exports) {

	var myCat3;

	this.Cat3 = (function() {
	  function Cat3() {}

	  Cat3.prototype.meow = function(s, count) {
	    var i, j, ref, result, voice;
	    voice = "にゃーん";
	    result = "";
	    count || (count = 1);
	    if (typeof s === "string") {
	      voice = s;
	    }
	    for (i = j = 1, ref = count; 1 <= ref ? j <= ref : j >= ref; i = 1 <= ref ? ++j : --j) {
	      result += i + voice;
	    }
	    return result;
	  };

	  Cat3.prototype.eat = function() {
	    this.length += 0.1;
	    return this.weight += 0.1;
	  };

	  return Cat3;

	})();

	myCat3 = new this.Cat3;

	myCat3.length = 30.5;

	myCat3.weight = 2.5;

	myCat3.eat();

	console.log("6-1:私の猫は" + (myCat3.meow()) + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");

	console.log("6-2:私の猫は" + (myCat3.meow("みゃお")) + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");

	console.log("6-3:私の猫は" + (myCat3.meow("みゃお", 3)) + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");


/***/ }
/******/ ]);