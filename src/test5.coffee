class Cat2
	meow: ->
		"にゃーん"
	eat: ->
		@length += 0.1;
		@weight += 0.1;

myCat2 = new Cat2
myCat2.length = 30.5
myCat2.weight = 2.5

myCat2.eat()

console.log "5:私の猫は#{myCat2.meow()}と鳴き、\n体長は#{myCat2.length}cm、体重は#{myCat2.weight}kgです"