class @Cat3
	meow: (s, count) ->
		voice = "にゃーん"
		result = ""
		count ||= 1
		
		if typeof(s) == "string"
			voice = s
		
		for i　in [1..count]
			result += i + voice
		
		result
	eat: ->
		@length += 0.1;
		@weight += 0.1;

myCat3 = new @Cat3
myCat3.length = 30.5
myCat3.weight = 2.5

myCat3.eat()

console.log "6-1:私の猫は#{myCat3.meow()}と鳴き、\n体長は#{myCat3.length}cm、体重は#{myCat3.weight}kgです"
console.log "6-2:私の猫は#{myCat3.meow("みゃお")}と鳴き、\n体長は#{myCat3.length}cm、体重は#{myCat3.weight}kgです"
console.log "6-3:私の猫は#{myCat3.meow("みゃお", 3)}と鳴き、\n体長は#{myCat3.length}cm、体重は#{myCat3.weight}kgです"